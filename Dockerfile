FROM python:3.10

ENV PYTHONUNBUFFERED 1

RUN pip install docker-compose==1.29.2 && pip install poetry==1.4.2 && poetry config virtualenvs.create false

RUN apt-get update && apt-get -y install cron


WORKDIR /usr/src/app
COPY pyproject.toml .
COPY poetry.lock .
RUN poetry install --no-root

COPY . .

EXPOSE 50051

RUN chmod +x /usr/src/app/app/cron_postgres_updater.py
RUN echo "0 3 * * * export $(cat /usr/src/app/.env.prod | xargs) && /usr/src/app/app/cron_postgres_updater.py 1> /tmp/out 2> /tmp/err\n" > /etc/cron.d/update_postgres
RUN chmod 0644 /etc/cron.d/update_postgres

CMD cron && crontab /etc/cron.d/update_postgres && touch /var/log/update_postgres.log && tail -f /var/log/update_postgres.log & alembic upgrade head && python app/main.py
