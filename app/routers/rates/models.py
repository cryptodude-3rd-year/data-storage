from datetime import datetime

from sqlalchemy import Column, Integer, DateTime, ForeignKey, Float
from sqlalchemy.orm import Mapped, relationship, mapped_column

from ..database import Base


class Rate(Base):

    __tablename__ = "rate"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    time = Column(DateTime, default=datetime.utcnow())
    sell_currency_id = mapped_column(ForeignKey("currency.id"), nullable=False)
    buy_currency_id = mapped_column(ForeignKey("currency.id"), nullable=False)
    exchange_id = mapped_column(ForeignKey("exchange.id"), nullable=False)
    price = Column(Float)

    sell_currency = relationship("Currency", foreign_keys=[sell_currency_id], lazy='subquery')
    buy_currency = relationship("Currency", foreign_keys=[buy_currency_id], lazy='subquery')
    exchange = relationship("Exchange", lazy='subquery')
