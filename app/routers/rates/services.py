from datetime import datetime
from typing import Sequence

from sqlalchemy import and_, desc
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from protos import data_storage_pb2
from .models import Rate


async def get_all_rates(session: AsyncSession) -> Sequence[Rate]:
    statement = select(Rate).order_by(desc(Rate.time))
    res = await session.scalars(statement)
    return res.all()


async def get_rates_by_ids(session: AsyncSession, ids: Sequence[int]) -> list[Rate]:
    result = []
    for index in ids:
        statement = select(Rate).where(Rate.id == index)
        res = await session.execute(statement)
        res = res.scalar()
        if res:
            result.append(res)
    return result


async def get_rates_by_filter(
        session: AsyncSession,
        start_time: datetime | None,
        end_time: datetime | None,
        exchange_ids: set[int],
        buy_currency_ids: set[int],
        sell_currency_ids: set[int]):
    if any([
        start_time != datetime(1970, 1, 1, 0, 0),
        end_time != datetime(1970, 1, 1, 0, 0),
        len(exchange_ids) > 0,
        len(buy_currency_ids) > 0,
        len(sell_currency_ids) > 0
    ]):
        statement = select(Rate).where(and_(
            start_time <= Rate.time if start_time != datetime(1970, 1, 1, 0, 0) else True,
            Rate.time <= end_time if end_time != datetime(1970, 1, 1, 0, 0) else True,
            Rate.exchange_id.in_(exchange_ids) if len(exchange_ids) > 0 else True,
            Rate.buy_currency_id.in_(buy_currency_ids) if len(buy_currency_ids) > 0 else True,
            Rate.sell_currency_id.in_(sell_currency_ids) if len(sell_currency_ids) > 0 else True
        )).order_by(desc(Rate.time))
    else:
        statement = select(Rate).order_by(desc(Rate.time))

    res = await session.scalars(statement)
    return res.all()


async def insert_rates(session: AsyncSession, rates: Sequence[data_storage_pb2.InsertRatesSingleRecord]) -> bool:
    rates_to_insert = list(map(
        lambda rate: Rate(
            sell_currency_id=rate.sell_currency_id,
            buy_currency_id=rate.buy_currency_id,
            exchange_id=rate.exchange_id,
            price=rate.price
        ),
        rates
    ))
    session.add_all(rates_to_insert)
    try:
        await session.commit()
        return True, "Success!"
    except Exception as e:
        return False, "Error! " + e.__str__()

