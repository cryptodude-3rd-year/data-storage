from typing import Sequence

from google.protobuf import timestamp_pb2
from protos import data_storage_pb2, data_storage_pb2_grpc
from .models import Rate
from .services import get_all_rates, get_rates_by_ids, get_rates_by_filter, insert_rates
from ..database import get_session


async def get_rates_to_send(rates: Sequence[Rate]) -> data_storage_pb2.GetRatesResponse:
    rates_to_send = []
    for record in rates:
        rt = timestamp_pb2.Timestamp()
        cbct = timestamp_pb2.Timestamp()
        cbut = timestamp_pb2.Timestamp()
        csct = timestamp_pb2.Timestamp()
        csut = timestamp_pb2.Timestamp()
        exct = timestamp_pb2.Timestamp()
        exut = timestamp_pb2.Timestamp()
        rt.FromDatetime(record.time)
        cbct.FromDatetime(record.buy_currency.created_at)
        cbut.FromDatetime(record.buy_currency.updated_at)
        csct.FromDatetime(record.sell_currency.created_at)
        csut.FromDatetime(record.sell_currency.updated_at)
        exct.FromDatetime(record.exchange.created_at)
        exut.FromDatetime(record.exchange.updated_at)

        rates_to_send.append(data_storage_pb2.RatesSingleRecord(
            id=record.id,
            sell_currency_id=record.sell_currency_id,
            buy_currency_id=record.buy_currency_id,
            exchange_id=record.exchange_id,
            price=record.price,
            time=rt,
            sell_currency=data_storage_pb2.CurrenciesSingleRecord(
                id=record.sell_currency.id,
                name=record.sell_currency.name,
                code_name=record.sell_currency.code_name,
                logo=record.sell_currency.logo,
                created_at=csct,
                updated_at=csut,
                is_active=record.sell_currency.is_active
            ),
            buy_currency=data_storage_pb2.CurrenciesSingleRecord(
                id=record.buy_currency.id,
                name=record.buy_currency.name,
                code_name=record.buy_currency.code_name,
                logo=record.buy_currency.logo,
                created_at=cbct,
                updated_at=cbut,
                is_active=record.buy_currency.is_active
            ),
            exchange=data_storage_pb2.ExchangesSingleRecord(
                id=record.exchange.id,
                name=record.exchange.name,
                url=record.exchange.url,
                logo=record.exchange.logo,
                maker_fee=record.exchange.maker_fee,
                taker_fee=record.exchange.taker_fee,
                fee_url=record.exchange.fee_url,
                created_at=exct,
                updated_at=exut,
                is_active=record.exchange.is_active
            )
        ))

    response = data_storage_pb2.GetRatesResponse(rates=rates_to_send)
    return response


class Rates(data_storage_pb2_grpc.RatesServicer):
    async def GetAllRates(self, request, context):
        async with get_session() as session:
            rates = await get_all_rates(session)
        response = await get_rates_to_send(rates)
        return response

    async def GetRatesByIDs(self, request, context):
        async with get_session() as session:
            rates = await get_rates_by_ids(session, request.ids)
        response = await get_rates_to_send(rates)
        return response

    async def GetRatesByFilter(self, request, context):
        start_time = request.start_time.ToDatetime()
        end_time = request.end_time.ToDatetime()
        exchange_ids = set(request.exchange_ids)
        buy_currency_ids = set(request.buy_currency_ids)
        sell_currency_ids = set(request.sell_currency_ids)

        async with get_session() as session:
            rates = await get_rates_by_filter(
                session, start_time, end_time, exchange_ids, buy_currency_ids, sell_currency_ids)

        response = await get_rates_to_send(rates)
        return response

    async def InsertRates(self, request, context):
        async with get_session() as session:
            success, message = await insert_rates(session, request.rates)
        response = data_storage_pb2.BlankResponse()
        response.success = success
        response.message = message
        return response
