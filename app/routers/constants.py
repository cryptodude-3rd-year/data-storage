from environs import Env

env = Env()
env.read_env(".env.prod")

GRPC_APP_HOST = env.str("GRPC_APP_HOST")
GRPC_APP_PORT = env.str("GRPC_APP_PORT")

DB_USER = env.str("POSTGRES_USER")
DB_PASSWORD = env.str("POSTGRES_PASSWORD")
DB_SERVER = env.str("DB_SERVER")
DB_NAME = env.str("POSTGRES_DB")

CRON_TIME_FROM_H = env.int("CRON_TIME_FROM_H")
CRON_TIME_TO_H = env.int("CRON_TIME_TO_H")
CRON_NEW_TIME_H = env.int("CRON_NEW_TIME_H")
