from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Float, Boolean
from sqlalchemy.orm import Mapped, relationship, mapped_column

from ..database import Base


class Exchange(Base):
    __tablename__ = "exchange"

    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True)
    url = Column(String, unique=True)
    logo = Column(String, default=None)
    maker_fee = Column(Float(precision=4), default=None)
    taker_fee = Column(Float(precision=4), default=None)
    fee_url = Column(String, default=None)
    is_active = Column(Boolean, default=True, index=True)
    created_at = Column(DateTime, default=datetime.utcnow())
    updated_at = Column(DateTime, default=datetime.utcnow(), onupdate=datetime.utcnow())
