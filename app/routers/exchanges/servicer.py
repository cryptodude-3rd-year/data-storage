from typing import Sequence

from google.protobuf import timestamp_pb2

from protos import data_storage_pb2, data_storage_pb2_grpc
from ..database import get_session
from .models import Exchange
from .services import (
    get_all_exchanges,
    get_exchanges_by_ids,
    get_exchanges_by_names,
    insert_exchanges,
    update_exchange,
    switch_state_of_exchange_by_id
)


async def get_exchanges_to_send(exchanges: Sequence[Exchange]) -> data_storage_pb2.GetExchangesResponse:
    exchanges_to_send = []
    for record in exchanges:
        created_at_ = timestamp_pb2.Timestamp()
        updated_at_ = timestamp_pb2.Timestamp()
        created_at_.FromDatetime(record.created_at)
        updated_at_.FromDatetime(record.updated_at)
        exchanges_to_send.append(data_storage_pb2.ExchangesSingleRecord(
            id=record.id,
            name=record.name,
            url=record.url,
            logo=record.logo,
            maker_fee=record.maker_fee,
            taker_fee=record.taker_fee,
            fee_url=record.fee_url,
            created_at=created_at_,
            updated_at=updated_at_,
            is_active=record.is_active
        ))

    response = data_storage_pb2.GetExchangesResponse(exchanges=exchanges_to_send)
    return response


class Exchanges(data_storage_pb2_grpc.ExchangesServicer):
    async def GetAllExchanges(self, request, context):
        include_inactive = bool(request.include_inactive)
        async with get_session() as session:
            exchanges = await get_all_exchanges(session, include_inactive)
        response = await get_exchanges_to_send(exchanges)
        return response

    async def GetExchangesByIDs(self, request, context):
        async with get_session() as session:
            exchanges = await get_exchanges_by_ids(session, request.ids)
        response = await get_exchanges_to_send(exchanges)
        return response

    async def GetExchangesByNames(self, request, context):
        async with get_session() as session:
            exchanges = await get_exchanges_by_names(session, request.names)
        response = await get_exchanges_to_send(exchanges)
        return response

    async def InsertExchanges(self, request, context):
        async with get_session() as session:
            success, message = await insert_exchanges(session, request.exchanges)
        response = data_storage_pb2.BlankResponse()
        response.success = success
        response.message = message
        return response

    async def UpdateExchange(self, request, context):
        async with get_session() as session:
            success, message = await update_exchange(session, request.exchange)
        response = data_storage_pb2.BlankResponse()
        response.success = success
        response.message = message
        return response

    async def ActivateExchangeById(self, request, context):
        async with get_session() as session:
            success, message = await switch_state_of_exchange_by_id(session, request.id, True)
        response = data_storage_pb2.BlankResponse()
        response.success = success
        response.message = message
        return response

    async def DeactivateExchangeById(self, request, context):
        async with get_session() as session:
            success, message = await switch_state_of_exchange_by_id(session, request.id, False)
        response = data_storage_pb2.BlankResponse()
        response.success = success
        response.message = message
        return response
