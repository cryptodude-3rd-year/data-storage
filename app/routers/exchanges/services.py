from typing import Sequence

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy import and_

from protos import data_storage_pb2
from .models import Exchange


async def get_all_exchanges(session: AsyncSession, include_inactive: bool = False) -> Sequence[Exchange]:
    if include_inactive:
        statement = select(Exchange)
    else:
        statement = select(Exchange).where(Exchange.is_active.is_(True))
    res = await session.scalars(statement)
    return res.all()


async def get_exchanges_by_ids(session: AsyncSession, ids: Sequence[int]) -> list[Exchange]:
    result = []
    for index in ids:
        statement = select(Exchange).where(and_(Exchange.id == index, Exchange.is_active.is_(True)))
        res = await session.execute(statement)
        res = res.scalar()
        if res:
            result.append(res)
    return result


async def get_exchanges_by_names(session: AsyncSession, names: Sequence[str]) -> list[Exchange]:
    result = []
    for name in names:
        statement = select(Exchange).where(and_(Exchange.name == name, Exchange.is_active.is_(True)))
        res = await session.execute(statement)
        res = res.scalar()
        if res:
            result.append(res)
    return result


async def insert_exchanges(
        session: AsyncSession, exchanges: Sequence[data_storage_pb2.InsertExchangesSingleRecord]) -> tuple[bool, str]:
    exchanges_to_insert = list(map(
        lambda exc: Exchange(
            id=exc.id,
            name=exc.name,
            url=exc.url,
            logo=exc.logo,
            maker_fee=exc.maker_fee,
            taker_fee=exc.taker_fee,
            fee_url=exc.fee_url,
        ),
        exchanges
    ))
    session.add_all(exchanges_to_insert)
    try:
        await session.commit()
        return True, "Success!"
    except Exception as e:
        return False, "Error! " + e.__str__()


async def update_exchange(
        session: AsyncSession, exchange: data_storage_pb2.InsertExchangesSingleRecord) -> tuple[bool, str]:
    statement = select(Exchange).where(and_(Exchange.id == exchange.id, Exchange.is_active.is_(True)))
    exchange_to_update: Exchange = await session.scalar(statement)
    if not exchange_to_update:
        return False, "Error! There is no such active exchange"
    if exchange.name:
        exchange_to_update.name = exchange.name
    if exchange.url:
        exchange_to_update.url = exchange.url
    if exchange.logo:
        exchange_to_update.logo = exchange.logo
    if exchange.maker_fee:
        exchange_to_update.maker_fee = exchange.maker_fee
    if exchange.logo:
        exchange_to_update.taker_fee = exchange.taker_fee
    if exchange.logo:
        exchange_to_update.fee_url = exchange.fee_url
    await session.commit()
    return True, "Success"


async def switch_state_of_exchange_by_id(session: AsyncSession, exchange_id: int, activate: bool) -> tuple[bool, str]:
    statement = select(Exchange).where(Exchange.id == exchange_id)
    res = await session.execute(statement)
    exc: Exchange = res.scalar()
    if not exc:
        return False, "Error! There is no such exchange"
    exc.is_active = activate
    await session.commit()
    return True, "Success"
