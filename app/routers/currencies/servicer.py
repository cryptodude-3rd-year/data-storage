from typing import Sequence

from google.protobuf import timestamp_pb2

from protos import data_storage_pb2, data_storage_pb2_grpc
from ..database import get_session

from .models import Currency
from .services import (
    get_all_currencies,
    get_currencies_by_ids,
    get_currencies_by_names,
    get_currencies_by_code_names,
    insert_currencies,
    update_currency, switch_state_of_currency_by_id
)


async def get_currencies_to_send(currencies: Sequence[Currency]) -> data_storage_pb2.GetCurrenciesResponse:
    currencies_to_send = []
    for record in currencies:
        created_at_ = timestamp_pb2.Timestamp()
        created_at_.FromDatetime(record.created_at)
        updated_at_ = timestamp_pb2.Timestamp()
        updated_at_.FromDatetime(record.updated_at)
        rec = data_storage_pb2.CurrenciesSingleRecord(
            id=record.id,
            name=record.name,
            code_name=record.code_name,
            logo=record.logo,
            created_at=created_at_,
            updated_at=updated_at_,
            is_active=record.is_active
        )

        currencies_to_send.append(rec)

    response = data_storage_pb2.GetCurrenciesResponse(currencies=currencies_to_send)
    return response


class Currencies(data_storage_pb2_grpc.CurrenciesServicer):
    async def GetAllCurrencies(self, request, context):
        include_inactive = bool(request.include_inactive)
        async with get_session() as session:
            currencies = await get_all_currencies(session, include_inactive)
        response = await get_currencies_to_send(currencies)
        return response

    async def GetCurrenciesByIDs(self, request, context):
        async with get_session() as session:
            currencies = await get_currencies_by_ids(session, request.ids)
        response = await get_currencies_to_send(currencies)
        return response

    async def GetCurrenciesByNames(self, request, context):
        async with get_session() as session:
            currencies = await get_currencies_by_names(session, request.names)
        response = await get_currencies_to_send(currencies)
        return response

    async def GetCurrenciesByCodeNames(self, request, context):
        async with get_session() as session:
            currencies = await get_currencies_by_code_names(session, request.code_names)
        response = await get_currencies_to_send(currencies)
        return response

    async def InsertCurrencies(self, request, context):
        async with get_session() as session:
            success, message = await insert_currencies(session, request.currencies)
        response = data_storage_pb2.BlankResponse()
        response.success = success
        response.message = message
        return response

    async def UpdateCurrency(self, request, context):
        async with get_session() as session:
            success, message = await update_currency(session, request.currency)
        response = data_storage_pb2.BlankResponse()
        response.success = success
        response.message = message
        return response

    async def ActivateCurrencyById(self, request, context):
        async with get_session() as session:
            success, message = await switch_state_of_currency_by_id(session, request.id, True)
        response = data_storage_pb2.BlankResponse()
        response.success = success
        response.message = message
        return response

    async def DeactivateCurrencyById(self, request, context):
        async with get_session() as session:
            success, message = await switch_state_of_currency_by_id(session, request.id, False)
        response = data_storage_pb2.BlankResponse()
        response.success = success
        response.message = message
        return response
