from typing import Sequence

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy import and_

from protos import data_storage_pb2
from .models import Currency


async def get_all_currencies(session: AsyncSession, include_inactive: bool = False) -> Sequence[Currency]:
    if include_inactive:
        statement = select(Currency)
    else:
        statement = select(Currency).where(Currency.is_active.is_(True))
    res = await session.scalars(statement)
    return res.all()


async def get_currencies_by_ids(session: AsyncSession, ids: Sequence[int]) -> list[Currency]:
    result = []
    for index in ids:
        statement = select(Currency).where(and_(Currency.id == index, Currency.is_active.is_(True)))
        res = await session.execute(statement)
        res = res.scalar()
        if res:
            result.append(res)
    return result


async def get_currencies_by_names(session: AsyncSession, names: Sequence[str]) -> list[Currency]:
    result = []
    for name in names:
        statement = select(Currency).where(and_(Currency.name == name, Currency.is_active.is_(True)))
        res = await session.execute(statement)
        res = res.scalar()
        if res:
            result.append(res)
    return result


async def get_currencies_by_code_names(session: AsyncSession, code_names: Sequence[str]) -> list[Currency]:
    result = []
    for code_name in code_names:
        statement = select(Currency).where(and_(Currency.code_name == code_name, Currency.is_active.is_(True)))
        res = await session.execute(statement)
        res = res.scalar()
        if res:
            result.append(res)
    return result


async def insert_currencies(
        session: AsyncSession, currencies: Sequence[data_storage_pb2.InsertCurrenciesSingleRecord]) -> tuple[bool, str]:
    currencies_to_insert = list(map(
        lambda cur: Currency(
            id=cur.id,
            name=cur.name,
            code_name=cur.code_name,
            logo=cur.logo
        ),
        currencies
    ))
    session.add_all(currencies_to_insert)
    try:
        await session.commit()
        return True, "Success!"
    except Exception as e:
        return False, "Error! " + e.__str__()


async def update_currency(
        session: AsyncSession, currency: data_storage_pb2.InsertCurrenciesSingleRecord) -> tuple[bool, str]:
    statement = select(Currency).where(and_(Currency.id == currency.id, Currency.is_active.is_(True)))
    currency_to_update: Currency = await session.scalar(statement)
    if not currency_to_update:
        return False, "Error! There is no such active currency"
    if currency.name:
        currency_to_update.name = currency.name
    if currency.code_name:
        currency_to_update.code_name = currency.code_name
    if currency.logo:
        currency_to_update.logo = currency.logo
    await session.commit()
    return True, "Success"


async def switch_state_of_currency_by_id(session: AsyncSession, currency_id: int, activate: bool) -> tuple[bool, str]:
    statement = select(Currency).where(Currency.id == currency_id)
    res = await session.execute(statement)
    cur: Currency = res.scalar()
    if not cur:
        return False, "Error! There is no such currency"
    cur.is_active = activate
    await session.commit()
    return True, "Success"
