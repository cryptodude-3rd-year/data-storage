from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean
from sqlalchemy.orm import mapped_column

from ..database import Base


class Currency(Base):
    __tablename__ = "currency"

    id = mapped_column(Integer, primary_key=True)
    name = Column(String, unique=True)
    code_name = Column(String, unique=True)
    logo = Column(String, default=None)
    is_active = Column(Boolean, default=True, index=True)
    created_at = Column(DateTime, default=datetime.utcnow())
    updated_at = Column(DateTime, default=datetime.utcnow(), onupdate=datetime.utcnow())
