import logging
import asyncio
from grpc import aio

from routers.constants import GRPC_APP_HOST, GRPC_APP_PORT
from protos import data_storage_pb2_grpc
from routers.exchanges.servicer import Exchanges
from routers.currencies.servicer import Currencies
from routers.rates.servicer import Rates


# Coroutines to be invoked when the event loop is shutting down.
_cleanup_coroutines = []


async def serve():
    server = aio.server()
    data_storage_pb2_grpc.add_RatesServicer_to_server(Rates(), server)
    data_storage_pb2_grpc.add_CurrenciesServicer_to_server(Currencies(), server)
    data_storage_pb2_grpc.add_ExchangesServicer_to_server(Exchanges(), server)
    listen_addr = f"{GRPC_APP_HOST}:{GRPC_APP_PORT}"
    server.add_insecure_port(listen_addr)
    logging.info("Starting server on %s", listen_addr)
    await server.start()

    async def server_graceful_shutdown():
        logging.info("Starting graceful shutdown...")
        # Shuts down the server with 5 seconds of grace period. During the
        # grace period, the server won't accept new connections and allow
        # existing RPCs to continue within the grace period.
        await server.stop(5)

    _cleanup_coroutines.append(server_graceful_shutdown())
    await server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(serve())
    finally:
        loop.run_until_complete(*_cleanup_coroutines)
        loop.close()
