#!/usr/local/bin/python
import asyncio
from datetime import datetime, timedelta

from sqlalchemy.future import select
from sqlalchemy import and_, desc, delete

from routers.constants import CRON_TIME_FROM_H, CRON_TIME_TO_H, CRON_NEW_TIME_H
from routers.database import get_session
from routers import Rate


async def main():
    time_from = datetime.now() - timedelta(hours=CRON_TIME_FROM_H)
    time_to = datetime.now() - timedelta(hours=CRON_TIME_TO_H)
    new_time = datetime.now() - timedelta(hours=CRON_NEW_TIME_H)

    statement = select(Rate.sell_currency_id, Rate.buy_currency_id, Rate.exchange_id).where(
        and_(time_from <= Rate.time, Rate.time <= time_to)).distinct()
    async with get_session() as session:
        result = await session.execute(statement)
        result = result.tuples()

    shit_to_add = []
    for sell_currency_id, buy_currency_id, exchange_id in result:
        stmt = select(Rate.price).order_by(desc(Rate.time)).where(and_(
            time_from <= Rate.time, Rate.time <= time_to, Rate.sell_currency_id == sell_currency_id,
            Rate.buy_currency_id == buy_currency_id, Rate.exchange_id == exchange_id))

        async with get_session() as session:
            result = await session.execute(stmt)
            prices = [p[0] for p in result.tuples()]
            price = sum(prices)/len(prices)

        shit_to_add.append(Rate(
            sell_currency_id=sell_currency_id,
            buy_currency_id=buy_currency_id,
            exchange_id=exchange_id,
            time=new_time,
            price=price
        ))

    deletion_statement = delete(Rate).where(and_(time_from <= Rate.time, Rate.time <= time_to))

    async with get_session() as session:
        await session.execute(deletion_statement)
        session.add_all(shit_to_add)
        await session.commit()


if __name__ == '__main__':
    print(CRON_TIME_FROM_H)
    asyncio.run(main())
